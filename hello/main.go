package main

import (
	"context"
	"net/http"

	server "gitlab.com/suffiks/saison/gen"
)

type Projects struct{}

func (p *Projects) List(ctx context.Context) ([]*server.Project, error) {
	projects := []*server.Project{
		{ID: "1234", Name: "Hello"},
		{ID: "asdf", Name: "World"},
		{ID: "yola", Name: "Yolo"},
	}

	return projects, nil
}

func (p *Projects) Get(ctx context.Context, id string) (*server.Project, error) {
	panic("not implemented")
}

func (p *Projects) Create(ctx context.Context, input *server.Project) (*server.Project, error) {
	panic("not implemented")
}

func (p *Projects) Update(ctx context.Context, id string, input *server.Project) (*server.Project, error) {
	panic("not implemented")
}

func (p *Projects) Delete(ctx context.Context, id string) error {
	panic("not implemented")
}

func main() {
	s := server.NewServer()

	s.RegisterProjectResolver(&Projects{})

	http.ListenAndServe(":3000", s)
}

module gitlab.com/suffiks/saison

require (
	github.com/alecthomas/participle v0.2.1-0.20190103085315-bf8340a459bd
	github.com/coreos/etcd v3.3.12+incompatible // indirect
	github.com/dave/jennifer v1.3.0
	github.com/davecgh/go-spew v1.1.1
	github.com/gedex/inflector v0.0.0-20170307190818-16278e9db813
	github.com/google/jsonapi v0.0.0-20181016150055-d0428f63eb51
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a
	github.com/julienschmidt/httprouter v1.2.0
	github.com/manyminds/api2go v0.0.0-20181019084807-186a8a2128df
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/spf13/afero v1.2.1 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/ugorji/go/codec v0.0.0-20190204201341-e444a5086c43 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4
	golang.org/x/sys v0.0.0-20190204203706-41f3e6584952 // indirect
	golang.org/x/tools v0.0.0-20190214204934-8dcb7bc8c7fe
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

replace github.com/google/jsonapi => github.com/bfitzsimmons/jsonapi v1.2.2

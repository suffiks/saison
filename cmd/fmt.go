package cmd

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/suffiks/saison/pkg/targets"
)

// fmtCmd represents the fmt command
var fmtCmd = &cobra.Command{
	Use:   "fmt",
	Short: "Format saison code",
	Long:  `Default formatting for saison code`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return errors.New("requires exactly one argument")
		}

		_, err := os.Stat(args[0])
		if err != nil {
			if os.IsNotExist(err) {
				return fmt.Errorf("%s not found", args[0])
			}
			return fmt.Errorf("Error: %s", err)
		}
		return nil
	},

	Run: func(cmd *cobra.Command, args []string) {
		s, err := parseFile(args[0])
		checkErr(err)
		formatting := &targets.Saison{}
		checkErr(formatting.Format(os.Stdout, s))
	},
}

func init() {
	rootCmd.AddCommand(fmtCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// fmtCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// fmtCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

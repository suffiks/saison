package cmd

// import (
// 	"errors"
// 	"fmt"
// 	"os"

// 	"github.com/spf13/cobra"
// )

// buildCmd represents the compile command
// var buildCmd = &cobra.Command{
// 	Use:   "build",
// 	Short: "Compile to protobuf",
// 	Args: func(cmd *cobra.Command, args []string) error {
// 		if len(args) < 1 {
// 			return errors.New("requires at least one arg")
// 		}

// 		for i, f := range args {
// 			_, err := os.Stat(f)
// 			if err != nil {
// 				if os.IsNotExist(err) {
// 					return fmt.Errorf("%s not found. Argument index %d", f, i)
// 				}
// 				return fmt.Errorf("Error at argument %d: %s", i, err)
// 			}
// 		}
// 		return nil
// 	},

// 	Long: `A longer description that spans multiple lines and likely contains examples
// and usage of using your command. For example:.`,

// Run: func(cmd *cobra.Command, args []string) {
// out := cmd.Flag("output").Value.String()
// combine, err := cmd.Flags().GetBool("combine")
// checkErr(err)
// inputs, err := convertArgsToFilepaths(args)
// checkErr(err)

// var targ target
// if out == "-" {
// 	targ = &stdout{}
// } else {
// 	st, err := os.Stat(out)
// 	if err != nil {
// 		fmt.Fprintln(os.Stderr, err)
// 		os.Exit(1)
// 	}
// 	if !st.IsDir() {
// 		fmt.Fprintf(os.Stderr, "%q is not a directory", out)
// 		os.Exit(1)
// 	}
// 	targ = &directory{dir: out}
// }

// if err := processFiles(inputs, combine, targ); err != nil {
// 	fmt.Fprintln(os.Stderr, err)
// 	os.Exit(1)
// }
// 	},
// }

// func init() {
// 	rootCmd.AddCommand(buildCmd)

// 	buildCmd.Flags().StringP("output", "o", "-", "Output directory. '-' for stdout")
// 	buildCmd.Flags().BoolP("combine", "c", false, "Combine all input files to single output file. Output should be output file")
// }

// func convertArgsToFilepaths(args []string) ([]string, error) {
// 	inputs := make([]string, 0, len(args))
// 	for _, f := range args {
// 		files, err := inputFiles(f)
// 		if err != nil {
// 			return nil, err
// 		}
// 		inputs = append(inputs, files...)
// 	}

// 	if len(inputs) == 0 {
// 		return nil, fmt.Errorf("no files found")
// 	}

// 	return inputs, nil
// }

// func inputFiles(input string) (files []string, err error) {
// 	st, err := os.Stat(input)
// 	if err != nil {
// 		return nil, err
// 	}

// 	if !st.IsDir() {
// 		return []string{input}, nil
// 	}

// 	fs, err := ioutil.ReadDir(input)
// 	if err != nil {
// 		return nil, err
// 	}

// 	for _, fi := range fs {
// 		if !fi.IsDir() && validFile(fi.Name()) {
// 			name := filepath.Join(input, fi.Name())
// 			files = append(files, name)
// 		}
// 	}
// 	return files, nil
// }

// func validFile(name string) bool {
// 	return filepath.Ext(name) == ".saison"
// }

// type target interface {
// 	NewFile(filename string) (io.WriteCloser, error)
// }

// type bufferedWriter struct {
// 	bytes.Buffer
// 	w io.Writer
// }

// func (s *bufferedWriter) Write(b []byte) (int, error) {
// 	return s.Buffer.Write(b)
// }

// func (s *bufferedWriter) Close() error {
// 	_, err := s.WriteTo(s.w)
// 	return err
// }

// type stdout struct{}

// func (s *stdout) NewFile(name string) (io.WriteCloser, error) {
// 	return &bufferedWriter{
// 		w: os.Stdout,
// 	}, nil
// }

// type directory struct {
// 	dir string
// }

// func (d *directory) NewFile(name string) (io.WriteCloser, error) {
// 	basename := filepath.Base(name)
// 	basename = basename[:len(basename)-len(filepath.Ext(basename))]
// 	basename += ".proto"
// 	return os.OpenFile(filepath.Join(d.dir, basename), os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
// }

// func processFiles(files []string, combine bool, trg target) error {
// 	// formatting := &targets.Protobuf{}
// 	// if combine {
// 	// 	return combineFiles(files, trg, formatting)
// 	// } else {
// 	// 	return processAsSeparateFiles(files, trg, formatting)
// 	// }
// 	return nil
// }

// func combineFiles(files []string, trg target, formatting targets.Target) error {
// 	combines := map[string][]*saison.Saison{}

// 	for _, file := range files {
// 		s, err := parseFile(file)
// 		if err != nil {
// 			return err
// 		}
// 		name := packageName(s)
// 		combines[name] = append(combines[name], s)
// 	}

// 	for pkg, sais := range combines {
// 		err := func() error {
// 			s := combine(sais...)
// 			wc, err := trg.NewFile(pkg + ".proto")
// 			if err != nil {
// 				return err
// 			}
// 			defer wc.Close()

// 			return formatting.Format(wc, s)
// 		}()
// 		if err != nil {
// 			return err
// 		}
// 	}
// 	return nil
// }

// func combine(incs ...*saison.Saison) *saison.Saison {
// 	base := &saison.Saison{}
// 	var (
// 		pkg *saison.Entry
// 		imp *saison.Entry
// 	)

// 	for _, in := range incs {
// 		for _, ent := range in.Entries {
// 			if ent.Package != "" {
// 				if pkg != nil {
// 					continue
// 				}
// 				pkg = ent
// 			}

// 			if ent.Imports != nil {
// 				if imp != nil {
// 					imp.Imports = append(imp.Imports, ent.Imports...)
// 					continue
// 				}
// 				imp = ent
// 			}

// 			base.Entries = append(base.Entries, ent)
// 		}
// 	}

// 	fixImports(&imp.Imports, true)
// 	fmt.Println("IMPORTS", &imp.Imports)
// 	return base
// }

// func processAsSeparateFiles(files []string, trg target, formatting targets.Target) error {
// 	grp, _ := errgroup.WithContext(context.Background())

// 	for _, file := range files {
// 		filename := file
// 		grp.Go(func() error {
// 			wc, err := trg.NewFile(filename)
// 			if err != nil {
// 				return err
// 			}
// 			defer wc.Close()
// 			return processFile(wc, formatting, filename)
// 		})
// 	}

// 	return grp.Wait()
// }

// func parseFile(filename string) (*saison.Saison, error) {
// 	f, err := os.OpenFile(filename, os.O_RDONLY, 0644)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer f.Close()

// 	return saison.Parse(f)
// }

// func processFile(w io.Writer, target targets.Target, filename string) error {
// 	s, err := parseFile(filename)
// 	if err != nil {
// 		return err
// 	}

// 	for _, entry := range s.Entries {
// 		if entry.Imports != nil {
// 			fixImports(&entry.Imports, false)
// 		}
// 	}

// 	return target.Format(w, s)
// }

// func packageName(s *saison.Saison) string {
// 	for _, e := range s.Entries {
// 		if e.Package != "" {
// 			return e.Package
// 		}
// 	}

// 	return "__noname__"
// }

// func fixImports(imps *[]string, combined bool) {
// 	sorted := append([]string{}, (*imps)...)
// 	sort.Strings(sorted)
// 	fmt.Println("OLDIMP", sorted)
// 	oimp := uniqString(sorted)
// 	nimp := []string{}
// 	for _, imp := range oimp {
// 		if strings.HasSuffix(imp, ".saison") {
// 			if combined {
// 				continue
// 			}

// 			imp = strings.TrimSuffix(imp, ".saison") + ".proto"
// 		}
// 		nimp = append(nimp, imp)
// 	}
// 	*imps = nimp
// }

// func uniqString(s []string) (n []string) {
// 	for i, str := range s {
// 		if i > 0 {
// 			if str == s[i-1] {
// 				continue
// 			}
// 		}
// 		n = append(n, str)
// 	}
// 	return n
// }

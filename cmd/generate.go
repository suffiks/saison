package cmd

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/suffiks/saison/pkg/compile"
	"gitlab.com/suffiks/saison/pkg/saison/linker"
	"golang.org/x/sync/errgroup"

	"github.com/spf13/cobra"
)

type targetFile struct {
	bytes.Buffer
	target   compile.Compiler
	filename string
}

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:     "generate",
	Aliases: []string{"g"},
	Short:   "Command group for generating code",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return errors.New("requires exactly one argument")
		}

		_, err := os.Stat(args[0])
		if err != nil {
			if os.IsNotExist(err) {
				return fmt.Errorf("%s not found", args[0])
			}
			return fmt.Errorf("Error: %s", err)
		}

		o, err := cmd.Flags().GetString("out")
		if err != nil {
			return nil
		}

		st, err := os.Stat(o)
		if err != nil {
			if os.IsNotExist(err) {
				return fmt.Errorf("%q doesn't exist for --out", o)
			}
			return err
		}

		if !st.IsDir() {
			return fmt.Errorf("--out must be a directory")
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		out, err := cmd.Flags().GetString("out")
		checkErr(err)
		s, err := parseFile(args[0])
		checkErr(err)

		grp, _ := errgroup.WithContext(context.Background())
		files := []*targetFile{}

		t := &targetFile{
			target:   &compile.Resolver{},
			filename: "resolvers.go",
		}
		files = append(files, t)
		grp.Go(format(s, t))

		if ok, _ := cmd.Flags().GetBool("jsonapi"); ok {
			t := &targetFile{
				target:   &compile.JSONAPI{},
				filename: "jsonapi.go",
			}
			files = append(files, t)
			grp.Go(format(s, t))
		}

		checkErr(grp.Wait())

		for _, tf := range files {
			checkErr(writeFile(out, tf))
		}
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)

	generateCmd.Flags().BoolP("jsonapi", "", false, "Generate a JSON api server")
	generateCmd.Flags().StringP("out", "o", "", "Output directory")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func format(l *linker.Linked, tf *targetFile) func() error {
	return func() error {
		return tf.target.Compile(tf, l)
	}
}

func writeFile(out string, tf *targetFile) error {
	f, err := os.OpenFile(filepath.Join(out, tf.filename), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(f, tf)
	return err
}

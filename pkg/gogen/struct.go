package gogen

import (
	"strings"

	"github.com/dave/jennifer/jen"
)

type Type struct {
	Ptr  bool
	Pkg  string
	Name string
}

func (t *Type) Gen() jen.Code {
	j := &jen.Statement{}
	if t.Ptr {
		j.Op("*")
	}

	if t.Pkg != "" {
		j.Qual(t.Pkg, t.Name)
	} else {
		j.Id(t.Name)
	}
	return j
}

type Struct struct {
	Name   string
	Fields []Field
	funcs  []Func
}

func (s *Struct) Add(f Func) {
	f.parent = s
	s.funcs = append(s.funcs, f)
}

func (s *Struct) Gen() jen.Code {
	j := jen.Type().Id(s.Name).StructFunc(func(g *jen.Group) {
		for _, f := range s.Fields {
			g.Add(f.Gen())
		}
	})

	for _, f := range s.funcs {
		j.Line().Add(f.Gen()).Line()
	}

	return j
}

type Field struct {
	Name string
	Type Type
	Tags map[string]string
}

func (f *Field) Gen() jen.Code {
	return jen.Id(f.Name).Add(f.Type.Gen()).Tag(f.Tags)
}

type Func struct {
	Name   string
	Params []Param
	Return []Param
	Block  func(g *jen.Group)
	parent *Struct
}

func (f *Func) Gen() jen.Code {
	j := jen.Func()

	if f.parent != nil {
		short := strings.ToLower(f.parent.Name)[0]
		j.Parens(jen.Id(string(short)).Op("*").Id(f.parent.Name))
	}

	if f.Name != "" {
		j.Id(f.Name)
	}

	f.base(j)

	if f.Block != nil {
		j.BlockFunc(f.Block)
	}

	return j
}

func (f *Func) base(j *jen.Statement) {
	j.ParamsFunc(func(g *jen.Group) {
		for _, p := range f.Params {
			g.Add(p.Gen())
		}
	})

	g := &jen.Group{}
	for _, r := range f.Return {
		g.Add(r.Gen())
	}
	j.Parens(g)
}

func (f *Func) GenType() jen.Code {
	t := jen.Type().Id(f.Name).Func()
	f.base(t)
	return t
}

type Param struct {
	Name string
	Type Type
}

func (p *Param) Gen() jen.Code {
	j := &jen.Statement{}
	if p.Name != "" {
		j.Id(p.Name)
	}

	j.Add(p.Type.Gen())
	return j
}

func String() Type { return Type{Name: "string"} }
func Int() Type    { return Type{Name: "int"} }
func Error() Type  { return Type{Name: "error"} }

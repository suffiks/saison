package targets

import (
	"io"
)

type Target interface {
	Format(w io.Writer, obj interface{}) error
}

package targets

import (
	"fmt"
	"io"
	"strings"
)

type syntaxWriter struct {
	w       io.Writer
	err     error
	indents int
	newline int
}

func (s *syntaxWriter) Write(v ...interface{}) {
	if s.err != nil {
		return
	}

	if s.newline > 0 {
		_, s.err = fmt.Fprint(s.w, strings.Repeat("\t", s.indents))
		if s.err != nil {
			return
		}
		s.newline = 0
	}
	_, s.err = fmt.Fprint(s.w, v...)
}

func (s *syntaxWriter) Writeln(v ...interface{}) {
	s.Write(v...)
	s.Newline()
}

func (s *syntaxWriter) Wrap(before, after string, f func()) {
	s.Write(before)
	s.Newline()
	s.indents++
	f()
	s.indents--
	s.EnsureNewline()
	s.Write(after)
}

func (s *syntaxWriter) Newline() {
	s.newline++
	if s.newline <= 2 {
		_, s.err = fmt.Fprint(s.w, "\n")
	}
}

func (s *syntaxWriter) Separate() {
	s.Newline()
	s.Newline()
}

func (s *syntaxWriter) EnsureNewline() {
	if s.newline == 0 {
		s.Newline()
	}
}

func (s *syntaxWriter) WriteIfSet(str string) {
	if len(str) > 0 {
		s.Write(str)
	}
}

func (s *syntaxWriter) WritelnIfSet(str string) {
	if len(str) > 0 {
		s.Writeln(str)
	}
}

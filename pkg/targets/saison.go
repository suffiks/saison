package targets

import (
	"fmt"
	"io"

	"gitlab.com/suffiks/saison/pkg/saison"
)

type Saison struct {
	err error
}

func (s *Saison) Format(w io.Writer, obj interface{}) error {
	sw := &syntaxWriter{w: w}
	s.format(sw, obj)
	if s.err != nil {
		return s.err
	}
	return sw.err
}

func (s *Saison) format(sw *syntaxWriter, obj interface{}) {
	if s.err != nil {
		return
	}

	switch o := obj.(type) {
	case *saison.Saison:
		s.saison(sw, o)
	case *saison.Package:
		if o != nil {
			s.pkg(sw, o)
		}
	case *saison.Entry:
		s.entry(sw, o)
	case *saison.Model:
		if o != nil {
			s.model(sw, o)
		}
	case *saison.Field:
		s.field(sw, o)
	case *saison.Type:
		s.typ(sw, o)
	case *saison.Expose:
		if o != nil {
			s.expose(sw, o)
		}
	default:
		s.err = fmt.Errorf("targets, saison: unknown object to format: %T", o)
		return
	}
	return
}

func (s *Saison) saison(sw *syntaxWriter, obj *saison.Saison) {
	for _, e := range obj.Entries {
		s.entry(sw, e)
	}
	sw.EnsureNewline()
}

func (s *Saison) entry(sw *syntaxWriter, e *saison.Entry) {
	s.format(sw, e.Package)
	s.format(sw, e.Model)
	s.format(sw, e.Expose)
}

func (s *Saison) pkg(sw *syntaxWriter, p *saison.Package) {
	sw.Writeln("package ", p.Name)
}

func (s *Saison) model(sw *syntaxWriter, m *saison.Model) {
	sw.Separate()
	sw.Write("model ", m.Name)
	sw.Wrap("{", "}", func() {
		for _, f := range m.Fields {
			s.format(sw, f)
			sw.EnsureNewline()
		}
	})
}

func (s *Saison) field(sw *syntaxWriter, f *saison.Field) {
	sw.Write(f.Name, " ")
	s.format(sw, f.Type)

	if f.Index != nil {
		sw.Write(" = ", *f.Index)
	}
}

func (s *Saison) typ(sw *syntaxWriter, t *saison.Type) {
	if t.Array {
		sw.Write("[]")
	}
	sw.Write(t.Type)
}

func (s *Saison) expose(sw *syntaxWriter, t *saison.Expose) {
	sw.Separate()
	sw.Write("expose ", t.Name)
	sw.Wrap(" {", "}", func() {
		for _, ignore := range t.Ignore {
			sw.Writeln(ignore)
		}
	})
}

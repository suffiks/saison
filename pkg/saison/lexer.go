package saison

import (
	"fmt"
	"io"
	"strconv"
	"text/scanner"
	"unicode/utf8"

	"github.com/alecthomas/participle/lexer"
)

type Comment struct {
	Pos  lexer.Position
	Text string
}

type saisonLexer struct {
	Comments []lexer.Token
	scanner  scanner.Scanner
	filename string
	err      error
}

func (s *saisonLexer) Lex(r io.Reader) (lexer.Lexer, error) {
	s.scanner = scanner.Scanner{}

	s.scanner.Init(r)
	s.scanner.Mode = scanner.ScanIdents | scanner.ScanInts | scanner.ScanStrings | scanner.ScanComments
	s.filename = lexer.NameOfReader(r)
	return s, nil //lexer.LexWithScanner(r, scn), nil
}

func (s *saisonLexer) Next() (lexer.Token, error) {
	typ := s.scanner.Scan()
	text := s.scanner.TokenText()
	pos := lexer.Position(s.scanner.Position)
	pos.Filename = s.filename
	if s.err != nil {
		return lexer.Token{}, s.err
	}
	return s.textScannerTransform(lexer.Token{
		Type:  typ,
		Value: text,
		Pos:   pos,
	})
}

func (s *saisonLexer) textScannerTransform(token lexer.Token) (lexer.Token, error) {
	// Unquote strings.
	switch token.Type {
	case scanner.Char:
		// FIXME(alec): This is pretty hacky...we convert a single quoted char into a double
		// quoted string in order to support single quoted strings.
		token.Value = fmt.Sprintf("\"%s\"", token.Value[1:len(token.Value)-1])
		fallthrough
	case scanner.String:
		s, err := strconv.Unquote(token.Value)
		if err != nil {
			return lexer.Token{}, lexer.Errorf(token.Pos, "%s: %q", err.Error(), token.Value)
		}
		token.Value = s
		if token.Type == scanner.Char && utf8.RuneCountInString(s) > 1 {
			token.Type = scanner.String
		}
	case scanner.RawString:
		token.Value = token.Value[1 : len(token.Value)-1]
	case scanner.Comment:
		s.Comments = append(s.Comments, token)
		return s.Next()
	}
	return token, nil
}

func (s *saisonLexer) Symbols() map[string]rune {
	return map[string]rune{
		"EOF":       scanner.EOF,
		"Char":      scanner.Char,
		"Ident":     scanner.Ident,
		"Int":       scanner.Int,
		"Float":     scanner.Float,
		"String":    scanner.String,
		"RawString": scanner.RawString,
		"Comment":   scanner.Comment,
	}
}

package linker

import (
	"go/types"
	"path/filepath"
	"strings"

	"github.com/gedex/inflector"
	"github.com/pkg/errors"
	"gitlab.com/suffiks/saison/pkg/saison"
	"golang.org/x/tools/go/packages"
)

type ModelList []*Model

func (m ModelList) Lookup(n string) *Model {
	for _, mod := range m {
		if mod.Name == n {
			return mod
		}
	}
	return nil
}

type Linked struct {
	PackageName string
	Models      ModelList
	Resolvers   []*Resolver
}

type Resolver struct {
	Name    string
	Model   *Model
	Methods []*Method
	addedTo map[*Model]struct{}
	ignores []string
}

func (r *Resolver) defaultMethods(model *Model, suffix, extraArgName string) {
	ctxType := func() *Type {
		return &Type{
			Pkg:     "context",
			Type:    "Context",
			VarName: "ctx",
		}
	}

	errType := func() *Type {
		return &Type{
			Type: "error",
		}
	}

	modelType := func(vname string, slice bool) *Type {
		t := &Type{
			VarName: vname,
			Type:    r.Model.Name,
			Slice:   slice,
			Ptr:     true,
		}
		if r.Model.Extends != "" {
			t.Pkg = r.Model.ExtendPackage
			t.Type = r.Model.Extends
		}
		return t
	}

	strType := func(vname string) *Type {
		return &Type{
			Type:    "string",
			VarName: vname,
		}
	}

	methods := []Method{
		Method{
			Type:   MethodList,
			Name:   "List",
			Args:   []*Type{ctxType()},
			Return: []*Type{modelType("", true), errType()},
			Model:  model,
		},
		Method{
			Type:   MethodGet,
			Name:   "Get",
			Args:   []*Type{ctxType(), strType("id")},
			Return: []*Type{modelType("", false), errType()},
			Model:  model,
		},
		Method{
			Type:   MethodCreate,
			Name:   "Create",
			Args:   []*Type{ctxType(), modelType("input", false)},
			Return: []*Type{modelType("", false), errType()},
			Model:  model,
		},
		Method{
			Type:   MethodUpdate,
			Name:   "Update",
			Args:   []*Type{ctxType(), strType("id"), modelType("input", false)},
			Return: []*Type{modelType("", false), errType()},
			Model:  model,
		},
		Method{
			Type:   MethodDelete,
			Name:   "Delete",
			Args:   []*Type{ctxType(), strType("id")},
			Return: []*Type{errType()},
			Model:  model,
		},
	}

OUTER:
	for _, met := range methods {
		met := met
		for _, ig := range r.ignores {
			if ig == met.Name {
				continue OUTER
			}
		}

		met.Name += suffix
		if extraArgName != "" {
			met.Args = append(met.Args, &Type{
				VarName: extraArgName,
				Type:    "string",
			})
		}
		r.Methods = append(r.Methods, &met)
	}
}

func (r *Resolver) addMethodsFor(mod *Model) {
	if r.addedTo == nil {
		r.addedTo = make(map[*Model]struct{})
	}

	if _, ok := r.addedTo[mod]; ok {
		return
	}

	r.defaultMethods(mod, "For"+mod.Name, strings.ToLower(mod.Name)+"ID")
}

type MethodType int

const (
	MethodList MethodType = iota
	MethodGet
	MethodCreate
	MethodUpdate
	MethodDelete
)

type Method struct {
	Type   MethodType
	Name   string
	Args   []*Type
	Return []*Type
	Model  *Model
}

type Type struct {
	VarName string
	Type    string
	Pkg     string
	Ptr     bool
	Slice   bool
}

type Model struct {
	Name          string
	Fields        []*Field
	Extends       string
	ExtendPackage string
	FromFunc      string
	ToFunc        string
}

func (m *Model) PluralizedName() string {
	return inflector.Pluralize(m.Name)
}

type Field struct {
	Name         string
	OriginalName string
	Slice        bool
	Type         string
	TargetModel  *Model
}

func (f *Field) Relation() bool {
	return f.TargetModel != nil
}

func Link(saisons ...*saison.Saison) (*Linked, error) {
	if len(saisons) == 0 {
		return nil, errors.Errorf("linker: must provide at least one saison document")
	}

	doc := saisons[0]
	for i := 1; i < len(saisons); i++ {
		if err := merge(doc, saisons[i]); err != nil {
			return nil, err
		}
	}

	l := &Linked{}
	for _, e := range doc.Entries {
		switch {
		case e.Package != nil:
			l.PackageName = e.Package.Name
		case e.Model != nil:
			m, err := genModel(e.Model)
			if err != nil {
				return nil, err
			}
			l.Models = append(l.Models, m)
		case e.Expose != nil:
			// Ignore here
			r, err := genResolver(e.Expose, l.Models)
			if err != nil {
				return nil, err
			}
			l.Resolvers = append(l.Resolvers, r)
		}
	}

	for _, mod := range l.Models {
		for _, fld := range mod.Fields {
			nm := l.Models.Lookup(fld.Type)
			fld.TargetModel = nm
			if !fld.Relation() {
				continue
			}

			ok := false
			for _, res := range l.Resolvers {
				if res.Model == nm {
					res.addMethodsFor(mod)
					ok = true
				}
			}
			if !ok {
				res := newResolver(nm, nil)
				res.addMethodsFor(mod)
				l.Resolvers = append(l.Resolvers, res)
			}
		}
	}

	return l, nil
}

func genModel(m *saison.Model) (*Model, error) {
	ret := &Model{
		Name: m.Name,
	}

	mappedFields := make(map[string]*Field, len(ret.Fields))
	for _, f := range m.Fields {
		name := f.Name
		if f.From != "" {
			name = f.From
		}

		field := &Field{
			OriginalName: f.From,
			Name:         f.Name,
			Type:         f.Type.Type,
			Slice:        f.Type.Array,
		}
		mappedFields[name] = field
		ret.Fields = append(ret.Fields, field)
	}

	if m.Extend == "" {
		return ret, nil
	}

	t, err := typeFor(m.Extend)
	if err != nil {
		return nil, errors.Wrapf(err, "error(%v)", m.Pos)
	}
	ret.Extends = t.Name() // t.Pkg().Name() + "." + t.Name()
	ret.ExtendPackage = t.Pkg().Path()
	ret.FromFunc = "from" + ret.Extends
	ret.ToFunc = "to" + ret.Name

	// TODO(thokra): validate field conversion

	return ret, nil
}

func genResolver(exp *saison.Expose, ml ModelList) (*Resolver, error) {
	model := ml.Lookup(exp.Name)
	if model == nil {
		return nil, errors.Errorf("error(%v): couln't find %v", exp.Pos, exp.Name)
	}

	res := newResolver(model, exp.Ignore)

	res.defaultMethods(model, "", "")

	return res, nil
}

func newResolver(model *Model, ignores []string) *Resolver {
	res := &Resolver{
		Name:    "Resolve" + model.PluralizedName(),
		Model:   model,
		ignores: ignores,
	}
	return res
}

func merge(l, r *saison.Saison) error {
	pkgname := ""
	for _, e := range l.Entries {
		if e.Package != nil {
			if pkgname != "" {
				return errors.Errorf("error(%v): package already defined", e.Package.Pos)
			}
			pkgname = e.Package.Name
		}
	}

	for _, e := range r.Entries {
		if e.Package != nil {
			if e.Package.Name != pkgname {
				return errors.Errorf("error(%v): another package name is already defined", e.Package.Pos)
			}
			continue
		}
		l.Entries = append(l.Entries, e)
	}
	return nil
}

func typeFor(typ string) (types.Object, error) {
	base := filepath.Base(typ)
	dir := filepath.Dir(typ)
	parts := strings.Split(base, ".")
	if len(parts) != 2 {
		return nil, errors.Errorf("extending must contain exactly one dot ('.')")
	}

	cnf := &packages.Config{
		Mode: packages.LoadTypes,
	}
	pkgs, err := packages.Load(cnf, filepath.Join(dir, parts[0]))
	if err != nil {
		return nil, err
	}

	pkg := pkgs[0]
	o := pkg.Types.Scope().Lookup(parts[1])
	if o == nil {
		return nil, errors.Errorf("type not found for %q", typ)
	}
	return o, nil
}

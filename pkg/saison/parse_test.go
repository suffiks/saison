package saison

import (
	"os"
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestScanner(t *testing.T) {
	filename := "testdata/apps.saison"
	f, err := os.OpenFile(filename, os.O_RDONLY, 0644)
	if err != nil {
		t.Fatal(err)
	}

	ret, err := Parse(f)
	if err != nil {
		t.Fatal(err)
	}

	spew.Dump(ret)
}

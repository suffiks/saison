package saison

import (
	"github.com/alecthomas/participle/lexer"
)

type Definition interface {
	Position() lexer.Position
}

type Saison struct {
	Pos      lexer.Position
	Comments []lexer.Token

	Entries []*Entry `( @@ )*`
}

type Entry struct {
	Pos lexer.Position

	Package *Package `  @@`
	Model   *Model   `| @@`
	Expose  *Expose  `| @@`
}

type Package struct {
	Pos lexer.Position

	Name string `"package" @Ident`
}

type Model struct {
	Pos lexer.Position

	Name   string   `"model" @Ident`
	Extend string   `(@String)?`
	Fields []*Field `"{" ( @@ )* "}"`
}

type Field struct {
	Pos lexer.Position

	Name  string `@Ident`
	From  string `( "(" @Ident ")" )?`
	Type  *Type  `@@`
	Index *int   `("=" @Int)?`
}

type Expose struct {
	Pos lexer.Position

	Name   string   `"expose" @(Ident | ".")+`
	Ignore []string `"{" ( @Ident )* "}"`
}

type Type struct {
	Array bool   `@("[" "]")?`
	Type  string `@Ident`
}

package saison

import (
	"io"

	"github.com/alecthomas/participle"
)

func Parse(f io.Reader) (*Saison, error) {
	lexer := &saisonLexer{}
	parser, err := participle.Build(&Saison{}, participle.Lexer(lexer))
	if err != nil {
		return nil, err
	}

	s := &Saison{}

	if err := parser.Parse(f, s); err != nil {
		return s, err
	}

	s.Comments = lexer.Comments

	return s, nil
}

package compile

import (
	"io"
	"log"
	"strings"

	"github.com/dave/jennifer/jen"
	"gitlab.com/suffiks/saison/pkg/saison/linker"
)

const (
	routerimp  = "github.com/julienschmidt/httprouter"
	jsonapiimp = "github.com/google/jsonapi"
)

type jsonAPIServer struct {
	*jen.Statement
}

func (j *jsonAPIServer) newFunc() *jen.Statement {
	f := jen.Func().Parens(jen.Id("s").Add(ptr).Id("JSONAPIServer"))
	j.Line().Line().Add(f)
	return f
}

type JSONAPI struct {
	link *linker.Linked
	f    *jen.File

	serverStruct *jsonAPIServer
}

func (j *JSONAPI) Compile(w io.Writer, link *linker.Linked) error {
	j.link = link
	j.f = jen.NewFile(link.PackageName)
	j.f.HeaderComment(pkgComment)

	j.f.Const().Id("jsonapiContentType").Op("=").Lit("application/vnd.api+json")

	if err := j.models(); err != nil {
		return err
	}

	if err := j.server(); err != nil {
		return err
	}

	if err := j.resolvers(); err != nil {
		return err
	}

	return j.f.Render(w)
}

func (j *JSONAPI) models() error {
	for _, mod := range j.link.Models {
		imname := "jsonapi" + mod.Name

		j.f.Type().Id(imname).StructFunc(func(g *jen.Group) {
			for _, field := range mod.Fields {
				tags := ""
				switch {
				case field.Name == "ID":
					tags += "primary," + jsonnormName(mod.PluralizedName())
				case field.Relation():
					tags += "relation," + jsonnormName(field.Name)
				default:
					tags += "attr," + jsonnormName(field.Name)
				}
				g.Add(formatField(field, "jsonapi")).Tag(map[string]string{"jsonapi": tags})
			}
			g.Line().Id("baseurl").String()
		})

		linkFunc := j.f.Func().Params(jen.Id("x").Add(ptr).Id(imname))
		linkFunc.Id("JSONAPILinks").Params().Add(ptr).Qual(jsonapiimp, "Links")
		linkFunc.BlockFunc(func(g *jen.Group) {
			g.Return(
				jen.Add(amp).Qual(jsonapiimp, "Links").Values(jen.Dict{
					jen.Lit("self"): jen.Qual("fmt", "Sprintf").Call(
						jen.Lit("%v/%v"),
						jen.Id("x").Dot("baseurl"),
						jen.Id("x").Dot("ID"),
					),
				}),
			)
		})

		linkRelFunc := j.f.Func().Params(jen.Id("x").Add(ptr).Id(imname))
		linkRelFunc.Id("JSONAPIRelationshipLinks").Params(jen.Id("name").String()).Add(ptr).Qual(jsonapiimp, "Links")
		linkRelFunc.BlockFunc(func(g *jen.Group) {
			g.Switch(jen.Id("name")).BlockFunc(func(sw *jen.Group) {
				for _, field := range mod.Fields {
					if !field.Relation() {
						continue
					}

					sw.Case(jen.Lit(jsonnormName(field.Name))).Block(
						jen.Return(
							jen.Add(amp).Qual(jsonapiimp, "Links").Values(jen.Dict{
								jen.Lit("related"): jen.Qual("fmt", "Sprintf").Call(
									jen.Lit("%v/%v/"+urlify(field.TargetModel.PluralizedName())),
									jen.Id("x").Dot("baseurl"),
									jen.Id("x").Dot("ID"),
								),
							}),
						),
					)
				}
			})
			g.Return(jen.Nil())
		})

		origType := jen.Id(mod.Name)
		if mod.Extends != "" {
			origType = jen.Qual(mod.ExtendPackage, mod.Extends)
		}

		tofunc := j.f.Func().Id("toJSONAPI" + mod.Name)
		tofunc.Params(jen.Id("v").Add(ptr).Add(origType), jen.Id("baseurl").String()).Add(ptr).Id(imname)
		tofunc.BlockFunc(func(g *jen.Group) {
			d := jen.Dict{
				jen.Id("baseurl"): jen.Id("baseurl"),
			}

			for _, f := range mod.Fields {
				if f.Relation() {
					continue
				}

				name := f.OriginalName
				if name == "" {
					name = f.Name
				}
				d[jen.Id(f.Name)] = jen.Id("v").Dot(name)
			}

			g.Return(jen.Add(amp).Id(imname).Values(d))
		})

		j.f.Line().Func().Id("fromJSONAPI" + mod.Name).Params(jen.Id("v").Add(ptr).Id(imname)).Add(ptr).Add(origType).BlockFunc(func(g *jen.Group) {
			d := jen.Dict{}

			for _, f := range mod.Fields {
				if f.Relation() {
					continue
				}

				name := f.OriginalName
				if name == "" {
					name = f.Name
				}
				d[jen.Id(name)] = jen.Id("v").Dot(f.Name)
			}

			g.Return(jen.Add(amp).Add(origType).Values(d))
		})
	}

	return nil
}

func (j *JSONAPI) server() error {
	const serverName = "JSONAPIServer"
	ss := j.f.Type().Id(serverName).StructFunc(func(g *jen.Group) {
		g.Id("Router").Add(ptr).Qual(routerimp, "Router")

		g.Line()
		for _, res := range j.link.Resolvers {
			g.Id("x" + res.Name).Id(res.Name)
		}
	})

	j.f.Func().Id("NewJSONAPI").Params().Add(ptr).Id(serverName).BlockFunc(func(g *jen.Group) {
		g.Return(jen.Add(amp).Id(serverName).Values(jen.DictFunc(func(d jen.Dict) {
			d[jen.Id("Router")] = jen.Qual(routerimp, "New").Call()
		})))
	})

	j.serverStruct = &jsonAPIServer{Statement: ss}

	pr := j.serverStruct.newFunc().Id("parseRequest")
	pr.Params(jen.Id("r").Add(ptr).Qual("net/http", "Request")).Qual("context", "Context")
	pr.BlockFunc(func(g *jen.Group) {
		g.Id("ctx").Op(":=").Id("r").Dot("Context").Call()

		g.Return(jen.Id("ctx"))
	})

	resp := j.serverStruct.newFunc().Id("respond")
	resp.Params(jen.Id("w").Qual("net/http", "ResponseWriter"), jen.Id("v").Interface(), jen.Id("status").Int())
	resp.BlockFunc(func(g *jen.Group) {
		g.Id("w").Dot("Header").Call().Dot("Add").Call(jen.Lit("Content-Type"), jen.Id("jsonapiContentType"))
		g.Id("w").Dot("WriteHeader").Call(jen.Id("status"))
		g.If(
			jen.Err().Op(":=").Qual(jsonapiimp, "MarshalPayload").Call(jen.Id("w"), jen.Id("v")),
			jen.Err().Op("!=").Nil(),
		).BlockFunc(func(ig *jen.Group) {
			ig.Qual("net/http", "Error").Call(jen.Id("w"), jen.Err().Dot("Error").Call(), jen.Lit(500))
			ig.Qual("log", "Println").Call(jen.Err())
		})
	})

	er := j.serverStruct.newFunc().Id("errorResponse")
	er.Params(jen.Id("w").Qual("net/http", "ResponseWriter"), jen.Err().Error())
	er.BlockFunc(func(g *jen.Group) {
		g.Id("w").Dot("Header").Call().Dot("Add").Call(jen.Lit("Content-Type"), jen.Id("jsonapiContentType"))
		g.Id("w").Dot("WriteHeader").Call(jen.Lit(500))
		g.Qual("log", "Println").Call(jen.Err())
	})

	return nil
}

func (j *JSONAPI) resolvers() error {
	for _, res := range j.link.Resolvers {
		s := j.serverStruct.newFunc()

		s.Id("Register" + res.Model.PluralizedName())
		s.Params(jen.Id("v").Id(res.Name)).BlockFunc(func(g *jen.Group) {
			g.Id("s").Dot("x" + res.Name).Op("=").Id("v")

			basePath := "/" + urlify(res.Model.PluralizedName())
			for _, met := range res.Methods {
				path := basePath
				if res.Model != met.Model {
					path = "/" + urlify(met.Model.PluralizedName()) + "/:" + modelToParamName(met.Model) + path
				}

				switch met.Type {
				case linker.MethodList:
					j.handleList(g, res, met, path)
				case linker.MethodGet:
					j.handleGet(g, res, met, path+"/:"+modelToParamName(res.Model))
				case linker.MethodCreate:
					j.handleDelete(g, res, met, path+"/:"+modelToParamName(res.Model))
					// g.Id("s").Dot("Router").Dot("POST").Call(jen.Lit(path), jen.Id("s").Dot(handlerName))
				case linker.MethodUpdate:
					// g.Id("s").Dot("Router").Dot("PUT").Call(jen.Lit(path+"/:id"), jen.Id("s").Dot(handlerName))
				case linker.MethodDelete:
					// g.Id("s").Dot("Router").Dot("DELETE").Call(jen.Lit(path+"/:id"), jen.Id("s").Dot(handlerName))
				default:
					log.Println("Unknown method type:", met.Type)
				}
			}
		})
	}

	return nil
}

func (j *JSONAPI) handleList(g *jen.Group, res *linker.Resolver, met *linker.Method, path string) {
	g.Add(j.handleGET(res, met, path, true))
}

func (j *JSONAPI) handleGet(g *jen.Group, res *linker.Resolver, met *linker.Method, path string) {
	g.Add(j.handleGET(res, met, path, false))
}

func (j *JSONAPI) handleGET(res *linker.Resolver, met *linker.Method, path string, isMany bool) jen.Code {
	handlerName := "handle" + res.Model.Name + met.Name

	r := jen.Id("s").Dot("Router").Dot("GET").Call(jen.Lit(path), jen.Id("s").Dot(handlerName))

	f := j.serverStruct.newFunc()
	f.Id(handlerName).Params(httpParams()...).BlockFunc(func(g *jen.Group) {
		j.handleStart(g, res, met, !isMany)

		g.Add(baseurlResolver(res.Model, "baseURL"))
		if isMany {
			g.Add(jsonapiMapMany(res.Model, "nv", "v", "baseURL"))
		} else {
			g.Id("nv").Op(":=").Add(jsonapiMapSingle(res.Model, "v", "baseURL"))
		}
		g.Id("s").Dot("respond").Call(jen.Id("w"), jen.Id("nv"), jen.Lit(200))
	})
	return r
}

func (j *JSONAPI) handleDelete(g *jen.Group, res *linker.Resolver, met *linker.Method, path string) {
	handlerName := "handle" + res.Model.Name + met.Name

	g.Id("s").Dot("Router").Dot("DELETE").Call(jen.Lit(path), jen.Id("s").Dot(handlerName))

	f := j.serverStruct.newFunc()
	f.Id(handlerName).Params(httpParams()...).BlockFunc(func(ig *jen.Group) {
		j.handleStart(ig, res, met, true)

		ig.Id("s").Dot("respond").Call(jen.Id("w"), jen.Nil(), jen.Lit(204))
	})
}

func (j *JSONAPI) handleStart(g *jen.Group, res *linker.Resolver, met *linker.Method, hasID bool) {
	g.Id("ctx").Op(":=").Id("s").Dot("parseRequest").Call(jen.Id("r"))

	callMethods := []jen.Code{
		jen.Id("ctx"),
	}
	if hasID {
		g.Id("objID").Op(":=").Id("ps").Dot("ByName").Call(jen.Lit(modelToParamName(res.Model)))
		callMethods = append(callMethods, jen.Id("objID"))
	}
	if res.Model != met.Model {
		g.Id("parent").Op(":=").Id("ps").Dot("ByName").Call(jen.Lit(modelToParamName(met.Model)))
		callMethods = append(callMethods, jen.Id("parent"))
	}

	g.List(jen.Id("v"), jen.Err()).Op(":=").Id("s").Dot("x" + res.Name).Dot(met.Name).Call(callMethods...)
	g.If(jen.Err().Op("!=").Nil()).Block(jen.Id("s").Dot("errorResponse").Call(jen.Id("w"), jen.Err()).Line().Return())
}

func httpParams() []jen.Code {
	return []jen.Code{
		jen.Id("w").Qual("net/http", "ResponseWriter"),
		jen.Id("r").Add(ptr).Qual("net/http", "Request"),
		jen.Id("ps").Qual(routerimp, "Params"),
	}
}

func urlify(s string) string {
	return strings.ToLower(s)
}

func jsonapiMapSingle(mod *linker.Model, oldVar, baseurlVar string) jen.Code {
	return jen.Id("toJSONAPI"+mod.Name).Call(jen.Id(oldVar), jen.Id(baseurlVar))
}

func jsonapiMapMany(mod *linker.Model, newVar, oldVar, baseurlVar string) jen.Code {
	vr := "sub" + oldVar
	x := jen.Var().Id(newVar).Index().Add(ptr).Id("jsonapi" + mod.Name).Line()
	return x.For(jen.List(jen.Id("_"), jen.Id(vr)).Op(":=").Range().Id(oldVar)).BlockFunc(func(g *jen.Group) {
		g.Id(newVar).Op("=").Append(jen.Id(newVar), jen.Id("toJSONAPI"+mod.Name).Call(jen.Id(vr), jen.Id(baseurlVar)))
	})
}

func baseurlResolver(model *linker.Model, varname string) jen.Code {
	return jen.Id(varname).Op(":=").Lit("http://").Op("+").Id("r").Dot("Host").Op("+").Lit("/" + urlify(model.PluralizedName()))
}

func jsonnormName(s string) string {
	return strings.ToLower(s)
}

func modelToParamName(m *linker.Model) string {
	return strings.ToLower(m.Name) + "_id"
}

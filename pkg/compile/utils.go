package compile

import (
	"io"

	"github.com/dave/jennifer/jen"
	"gitlab.com/suffiks/saison/pkg/saison/linker"
)

var (
	ptr = jen.Op("*")
	amp = jen.Op("&")
)

const (
	pkgComment = "GENERATED CODE, DO NOT EDIT"
)

type Compiler interface {
	Compile(w io.Writer, link *linker.Linked) error
}

func formatType(t *linker.Type) jen.Code {
	x := jen.Id(t.VarName)
	if t.Slice {
		x.Index()
	}

	if t.Ptr {
		x.Add(ptr)
	}

	if t.Pkg != "" {
		x.Qual(t.Pkg, t.Type)
	} else {
		x.Id(t.Type)
	}
	return x
}

func formatField(f *linker.Field, prefix ...string) jen.Code {
	typePrefix := ""
	if len(prefix) > 0 {
		typePrefix = prefix[0]
	}

	x := jen.Id(f.Name)
	if f.Slice {
		x.Index()
	}

	typID := f.Type
	if f.Relation() {
		x.Add(ptr)
		typID = typePrefix + typID
	}

	return x.Id(typID)
}

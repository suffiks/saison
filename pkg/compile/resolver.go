package compile

import (
	"io"

	"github.com/dave/jennifer/jen"
	"gitlab.com/suffiks/saison/pkg/saison/linker"
)

type Resolver struct {
	link *linker.Linked
	f    *jen.File
}

func (r *Resolver) Compile(w io.Writer, link *linker.Linked) error {
	r.link = link
	r.f = jen.NewFile(link.PackageName)
	r.f.HeaderComment(pkgComment)

	if err := r.models(); err != nil {
		return err
	}

	if err := r.resolvers(); err != nil {
		return err
	}

	return r.f.Render(w)
}

func (r *Resolver) models() error {
	for _, m := range r.link.Models {
		r.f.Add(jen.Type().Id(m.Name).StructFunc(func(g *jen.Group) {
			for _, f := range m.Fields {
				if f.Relation() {
					continue
				}

				g.Add(formatField(f))
			}
		})).Line()

		if m.Extends == "" {
			continue
		}

		r.f.Func().Id(m.FromFunc).Params(
			jen.Id("v").Add(ptr).Qual(m.ExtendPackage, m.Extends),
		).Add(jen.Add(ptr).Id(m.Name)).
			BlockFunc(func(g *jen.Group) {
				d := jen.Dict{}

				for _, f := range m.Fields {
					if f.Relation() {
						continue
					}

					name := f.OriginalName
					if name == "" {
						name = f.Name
					}
					d[jen.Id(f.Name)] = jen.Id("v").Dot(name)
				}
				r := jen.Add(amp).Id(m.Name).Values(d)
				g.Return(r)
			}).Line()

		r.f.Func().Id(m.ToFunc).Params(
			jen.Id("v").Add(ptr).Id(m.Name),
		).Add(ptr).Qual(m.ExtendPackage, m.Extends).
			BlockFunc(func(g *jen.Group) {
				d := jen.Dict{}

				for _, f := range m.Fields {
					if f.Relation() {
						continue
					}

					name := f.OriginalName
					if name == "" {
						name = f.Name
					}
					d[jen.Id(name)] = jen.Id("v").Dot(f.Name)
				}
				r := jen.Add(amp).Qual(m.ExtendPackage, m.Extends).Values(d)
				g.Return(r)
			}).Line()
	}

	return nil
}

func (r *Resolver) resolvers() error {
	for _, res := range r.link.Resolvers {
		r.f.Type().Id(res.Name).InterfaceFunc(func(g *jen.Group) {
			for _, met := range res.Methods {
				g.Id(met.Name).ParamsFunc(func(pg *jen.Group) {
					for _, arg := range met.Args {
						pg.Add(formatType(arg))
					}
				}).Parens(jen.ListFunc(func(rg *jen.Group) {
					for _, ret := range met.Return {
						rg.Add(formatType(ret))
					}
				}))
			}
		}).Line()
	}

	return nil
}

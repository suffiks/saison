// This file is generated - do not edit.

package server

import "context"

type ctxKey int

type Project struct {
	ID   string `json:"-"`
	Name string `json:"name,omitempty"`
}

type User struct {
	ID    string `json:"-"`
	Email string `json:"email,omitempty"`
	Name  string `json:"name,omitempty"`
}

type ProjectResolver interface {
	List(ctx context.Context) ([]*Project, error)
	Get(ctx context.Context, id string) (*Project, error)
	Create(ctx context.Context, input *Project) (*Project, error)
	Update(ctx context.Context, id string, input *Project) (*Project, error)
	Delete(ctx context.Context, id string) error
}

type UserResolver interface {
	List(ctx context.Context) ([]*User, error)
	Get(ctx context.Context, id string) (*User, error)
	Create(ctx context.Context, input *User) (*User, error)
}

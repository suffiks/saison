// This file is generated - do not edit.

package server

import (
	"context"
	"fmt"
	httprouter "github.com/julienschmidt/httprouter"
	jsonapi "github.com/manyminds/api2go/jsonapi"
	"net/http"
	"strings"
)

const ctxKeyIncludes ctxKey = 100

func (x *Project) GetID() string {
	return x.ID
}

func (x *User) GetID() string {
	return x.ID
}

type BaseURLFunc func(req *http.Request) string

func baseURLFromHost(r *http.Request) string {
	return r.URL.Host
}

func baseURLFromString(uri string) BaseURLFunc {
	return func(r *http.Request) string {
		return uri
	}
}

func NewServerWithBaseURLFunc(baseURLFunc BaseURLFunc) *JSONAPIServer {
	js := &JSONAPIServer{baseURL: baseURLFunc}
	js.init()
	return js
}

func NewServerWithBaseURL(baseURL string) *JSONAPIServer {
	return NewServerWithBaseURLFunc(baseURLFromString(baseURL))
}

// NewServer returns a new server
func NewServer() *JSONAPIServer {
	return NewServerWithBaseURLFunc(baseURLFromHost)
}

// HTTPError wraps a normal error with a status code used for responses
type HTTPError struct {
	StatusCode int
	Err        error
}

func (h *HTTPError) Error() string {
	return h.Err.Error()
}

func respondWith(w http.ResponseWriter, v interface{}, err error) {
	w.Header().Set("Content-Type", "application/vnd.api+json")
	if err != nil {
		if he, ok := err.(*HTTPError); ok {
			w.WriteHeader(he.StatusCode)
		}
		fmt.Fprintln(w, err)
		return
	}

	if b, err := jsonapi.Marshal(v); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		w.Write(b)
	}
}

func ctxFromRequest(r *http.Request) context.Context {
	ctx := r.Context()
	if include := r.URL.Query().Get("include"); include != "" {
		parts := strings.Split(include, ",")
		ctx = context.WithValue(ctx, ctxKeyIncludes, parts)
	}
	return ctx
}

type JSONAPIServer struct {
	baseURL        BaseURLFunc
	router         *httprouter.Router
	resolveProject ProjectResolver
	resolveUser    UserResolver
}

func (j *JSONAPIServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	j.router.ServeHTTP(w, r)
}

func (j *JSONAPIServer) RegisterProjectResolver(v ProjectResolver) {
	j.resolveProject = v
}

func (j *JSONAPIServer) httpListProject(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if j.resolveProject == nil {
		http.Error(w, "Not registered", http.StatusNotFound)
		return
	}
	ctx := ctxFromRequest(r)
	v, err := j.resolveProject.List(ctx)
	respondWith(w, v, err)
}

func (j *JSONAPIServer) RegisterUserResolver(v UserResolver) {
	j.resolveUser = v
}

func (j *JSONAPIServer) httpListUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if j.resolveUser == nil {
		http.Error(w, "Not registered", http.StatusNotFound)
		return
	}
	ctx := ctxFromRequest(r)
	v, err := j.resolveUser.List(ctx)
	respondWith(w, v, err)
}

func (j *JSONAPIServer) init() {
	r := httprouter.New()
	r.GET("/projects", j.httpListProject)
	r.GET("/users", j.httpListUser)
	j.router = r
}

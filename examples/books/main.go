package main

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/suffiks/saison/examples/books/books"
	genbooks "gitlab.com/suffiks/saison/examples/books/books/gen"
)

type storage struct {
	books   []*books.Book
	authors []*genbooks.Author
}

type booksController struct {
	s *storage
}

func (b *booksController) List(ctx context.Context) ([]*books.Book, error) {
	return b.s.books, nil
}

func (b *booksController) DeleteForAuthor(ctx context.Context, id, authorID string) error {
	return nil
}

func (b *booksController) Delete(ctx context.Context, id string) error {
	return nil
}

func (b *booksController) GetForAuthor(ctx context.Context, id, authorID string) (*books.Book, error) {
	return b.Get(ctx, id)
}

func (b *booksController) Get(ctx context.Context, id string) (*books.Book, error) {
	for _, book := range b.s.books {
		if book.IBAN == id {
			return book, nil
		}
	}

	return nil, fmt.Errorf("Not found")
}

func (b *booksController) ListForAuthor(ctx context.Context, authorID string) ([]*books.Book, error) {
	bks := []*books.Book{}
	for _, bk := range b.s.books {
		if bk.AuthorID == authorID {
			bks = append(bks, bk)
		}
	}
	return bks, nil
}

type authorsController struct {
	s *storage
}

func (a *authorsController) List(ctx context.Context) ([]*genbooks.Author, error) {
	return a.s.authors, nil
}

func (a *authorsController) DeleteForBook(ctx context.Context, id, bookID string) error {
	return nil
}

func (a *authorsController) Delete(ctx context.Context, id string) error {
	return nil
}

func (a *authorsController) GetForBook(ctx context.Context, id, bookID string) (*genbooks.Author, error) {
	return a.Get(ctx, id)
}

func (a *authorsController) Get(ctx context.Context, id string) (*genbooks.Author, error) {
	for _, auth := range a.s.authors {
		if auth.ID == id {
			return auth, nil
		}
	}

	return nil, fmt.Errorf("Not found")
}

func (a *authorsController) ListForBook(ctx context.Context, bookID string) ([]*genbooks.Author, error) {
	return nil, nil
}

func main() {
	store := &storage{
		books: []*books.Book{
			{IBAN: "23424", Name: "Hello World", AuthorID: "1"},
			{IBAN: "23425", Name: "Hello Thomas", AuthorID: "2"},
		},
		authors: []*genbooks.Author{
			{ID: "1", Name: "Thomas"},
			{ID: "2", Name: "Gunnar"},
		},
	}

	s := genbooks.NewJSONAPI()
	s.RegisterBooks(&booksController{store})
	s.RegisterAuthors(&authorsController{store})

	fmt.Println("Listening on :3000")
	http.ListenAndServe(":3000", s.Router)
}

package books

type Book struct {
	IBAN        string
	Name        string
	Description string
	AuthorID    string
}

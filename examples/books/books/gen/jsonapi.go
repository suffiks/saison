// GENERATED CODE, DO NOT EDIT

package genbooks

import (
	"context"
	"fmt"
	jsonapi "github.com/google/jsonapi"
	httprouter "github.com/julienschmidt/httprouter"
	books "gitlab.com/suffiks/saison/examples/books/books"
	"log"
	"net/http"
)

const jsonapiContentType = "application/vnd.api+json"

type jsonapiBook struct {
	ID          string         `jsonapi:"primary,books"`
	Title       string         `jsonapi:"attr,title"`
	Description string         `jsonapi:"attr,description"`
	Author      *jsonapiAuthor `jsonapi:"relation,author"`

	baseurl string
}

func (x *jsonapiBook) JSONAPILinks() *jsonapi.Links {
	return &jsonapi.Links{"self": fmt.Sprintf("%v/%v", x.baseurl, x.ID)}
}
func (x *jsonapiBook) JSONAPIRelationshipLinks(name string) *jsonapi.Links {
	switch name {
	case "author":
		return &jsonapi.Links{"related": fmt.Sprintf("%v/%v/authors", x.baseurl, x.ID)}
	}
	return nil
}
func toJSONAPIBook(v *books.Book, baseurl string) *jsonapiBook {
	return &jsonapiBook{
		Description: v.Description,
		ID:          v.IBAN,
		Title:       v.Name,
		baseurl:     baseurl,
	}
}

func fromJSONAPIBook(v *jsonapiBook) *books.Book {
	return &books.Book{
		Description: v.Description,
		IBAN:        v.ID,
		Name:        v.Title,
	}
}

type jsonapiAuthor struct {
	ID    string         `jsonapi:"primary,authors"`
	Name  string         `jsonapi:"attr,name"`
	Books []*jsonapiBook `jsonapi:"relation,books"`

	baseurl string
}

func (x *jsonapiAuthor) JSONAPILinks() *jsonapi.Links {
	return &jsonapi.Links{"self": fmt.Sprintf("%v/%v", x.baseurl, x.ID)}
}
func (x *jsonapiAuthor) JSONAPIRelationshipLinks(name string) *jsonapi.Links {
	switch name {
	case "books":
		return &jsonapi.Links{"related": fmt.Sprintf("%v/%v/books", x.baseurl, x.ID)}
	}
	return nil
}
func toJSONAPIAuthor(v *Author, baseurl string) *jsonapiAuthor {
	return &jsonapiAuthor{
		ID:      v.ID,
		Name:    v.Name,
		baseurl: baseurl,
	}
}

func fromJSONAPIAuthor(v *jsonapiAuthor) *Author {
	return &Author{
		ID:   v.ID,
		Name: v.Name,
	}
}

type JSONAPIServer struct {
	Router *httprouter.Router

	xResolveBooks   ResolveBooks
	xResolveAuthors ResolveAuthors
}

func (s *JSONAPIServer) parseRequest(r *http.Request) context.Context {
	ctx := r.Context()
	return ctx
}

func (s *JSONAPIServer) respond(w http.ResponseWriter, v interface{}, status int) {
	w.Header().Add("Content-Type", jsonapiContentType)
	w.WriteHeader(status)
	if err := jsonapi.MarshalPayload(w, v); err != nil {
		http.Error(w, err.Error(), 500)
		log.Println(err)
	}
}

func (s *JSONAPIServer) errorResponse(w http.ResponseWriter, err error) {
	w.Header().Add("Content-Type", jsonapiContentType)
	w.WriteHeader(500)
	log.Println(err)
}

func (s *JSONAPIServer) RegisterBooks(v ResolveBooks) {
	s.xResolveBooks = v
	s.Router.GET("/books", s.handleBookList)
	s.Router.GET("/books/:book_id", s.handleBookGet)
	s.Router.GET("/authors/:author_id/books", s.handleBookListForAuthor)
	s.Router.GET("/authors/:author_id/books/:book_id", s.handleBookGetForAuthor)
}

func (s *JSONAPIServer) handleBookList(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := s.parseRequest(r)
	v, err := s.xResolveBooks.List(ctx)
	if err != nil {
		s.errorResponse(w, err)
		return
	}
	baseURL := "http://" + r.Host + "/books"
	var nv []*jsonapiBook
	for _, subv := range v {
		nv = append(nv, toJSONAPIBook(subv, baseURL))
	}
	s.respond(w, nv, 200)
}

func (s *JSONAPIServer) handleBookGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := s.parseRequest(r)
	objID := ps.ByName("book_id")
	v, err := s.xResolveBooks.Get(ctx, objID)
	if err != nil {
		s.errorResponse(w, err)
		return
	}
	baseURL := "http://" + r.Host + "/books"
	nv := toJSONAPIBook(v, baseURL)
	s.respond(w, nv, 200)
}

func (s *JSONAPIServer) handleBookListForAuthor(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := s.parseRequest(r)
	parent := ps.ByName("author_id")
	v, err := s.xResolveBooks.ListForAuthor(ctx, parent)
	if err != nil {
		s.errorResponse(w, err)
		return
	}
	baseURL := "http://" + r.Host + "/books"
	var nv []*jsonapiBook
	for _, subv := range v {
		nv = append(nv, toJSONAPIBook(subv, baseURL))
	}
	s.respond(w, nv, 200)
}

func (s *JSONAPIServer) handleBookGetForAuthor(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := s.parseRequest(r)
	objID := ps.ByName("book_id")
	parent := ps.ByName("author_id")
	v, err := s.xResolveBooks.GetForAuthor(ctx, objID, parent)
	if err != nil {
		s.errorResponse(w, err)
		return
	}
	baseURL := "http://" + r.Host + "/books"
	nv := toJSONAPIBook(v, baseURL)
	s.respond(w, nv, 200)
}

func (s *JSONAPIServer) RegisterAuthors(v ResolveAuthors) {
	s.xResolveAuthors = v
	s.Router.GET("/authors", s.handleAuthorList)
	s.Router.GET("/authors/:author_id", s.handleAuthorGet)
	s.Router.GET("/books/:book_id/authors", s.handleAuthorListForBook)
	s.Router.GET("/books/:book_id/authors/:author_id", s.handleAuthorGetForBook)
}

func (s *JSONAPIServer) handleAuthorList(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := s.parseRequest(r)
	v, err := s.xResolveAuthors.List(ctx)
	if err != nil {
		s.errorResponse(w, err)
		return
	}
	baseURL := "http://" + r.Host + "/authors"
	var nv []*jsonapiAuthor
	for _, subv := range v {
		nv = append(nv, toJSONAPIAuthor(subv, baseURL))
	}
	s.respond(w, nv, 200)
}

func (s *JSONAPIServer) handleAuthorGet(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := s.parseRequest(r)
	objID := ps.ByName("author_id")
	v, err := s.xResolveAuthors.Get(ctx, objID)
	if err != nil {
		s.errorResponse(w, err)
		return
	}
	baseURL := "http://" + r.Host + "/authors"
	nv := toJSONAPIAuthor(v, baseURL)
	s.respond(w, nv, 200)
}

func (s *JSONAPIServer) handleAuthorListForBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := s.parseRequest(r)
	parent := ps.ByName("book_id")
	v, err := s.xResolveAuthors.ListForBook(ctx, parent)
	if err != nil {
		s.errorResponse(w, err)
		return
	}
	baseURL := "http://" + r.Host + "/authors"
	var nv []*jsonapiAuthor
	for _, subv := range v {
		nv = append(nv, toJSONAPIAuthor(subv, baseURL))
	}
	s.respond(w, nv, 200)
}

func (s *JSONAPIServer) handleAuthorGetForBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := s.parseRequest(r)
	objID := ps.ByName("author_id")
	parent := ps.ByName("book_id")
	v, err := s.xResolveAuthors.GetForBook(ctx, objID, parent)
	if err != nil {
		s.errorResponse(w, err)
		return
	}
	baseURL := "http://" + r.Host + "/authors"
	nv := toJSONAPIAuthor(v, baseURL)
	s.respond(w, nv, 200)
}
func NewJSONAPI() *JSONAPIServer {
	return &JSONAPIServer{Router: httprouter.New()}
}

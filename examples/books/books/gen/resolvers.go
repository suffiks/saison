// GENERATED CODE, DO NOT EDIT

package genbooks

import (
	"context"
	books "gitlab.com/suffiks/saison/examples/books/books"
)

type Book struct {
	ID          string
	Title       string
	Description string
}

func fromBook(v *books.Book) *Book {
	return &Book{
		Description: v.Description,
		ID:          v.IBAN,
		Title:       v.Name,
	}
}

func toBook(v *Book) *books.Book {
	return &books.Book{
		Description: v.Description,
		IBAN:        v.ID,
		Name:        v.Title,
	}
}

type Author struct {
	ID   string
	Name string
}

type ResolveBooks interface {
	List(ctx context.Context) ([]*books.Book, error)
	Get(ctx context.Context, id string) (*books.Book, error)
	Delete(ctx context.Context, id string) error
	ListForAuthor(ctx context.Context, authorID string) ([]*books.Book, error)
	GetForAuthor(ctx context.Context, id string, authorID string) (*books.Book, error)
	DeleteForAuthor(ctx context.Context, id string, authorID string) error
}

type ResolveAuthors interface {
	List(ctx context.Context) ([]*Author, error)
	Get(ctx context.Context, id string) (*Author, error)
	Delete(ctx context.Context, id string) error
	ListForBook(ctx context.Context, bookID string) ([]*Author, error)
	GetForBook(ctx context.Context, id string, bookID string) (*Author, error)
	DeleteForBook(ctx context.Context, id string, bookID string) error
}
